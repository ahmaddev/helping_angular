import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { AppRouting } from "./app.router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { GrowlModule } from "primeng/growl";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EditorModule } from "primeng/editor";
import { ChartModule } from "primeng/chart";

import { TagInputModule } from "ngx-chips";

import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { ToastModule } from "primeng/toast";
import { MessageService } from "primeng/api";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { HeaderComponent } from "./header/header.component";
import { TeachersComponent } from "./teachers/teachers.component";
import { StudentsComponent } from "./students/students.component";
import { SingleteacherComponent } from "./singleteacher/singleteacher.component";
import { SinglestudentComponent } from "./singlestudent/singlestudent.component";
import { AdminQueriesComponent } from "./admin-queries/admin-queries.component";
import { SingleAdminQueryComponent } from "./single-admin-query/single-admin-query.component";
import { NewTeachersComponent } from "./new-teachers/new-teachers.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    TeachersComponent,
    StudentsComponent,
    SingleteacherComponent,
    SinglestudentComponent,
    AdminQueriesComponent,
    SingleAdminQueryComponent,
    NewTeachersComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    FormsModule,
    ReactiveFormsModule,
    GrowlModule,
    EditorModule,
    TagInputModule,
    NgMultiSelectDropDownModule,
    ToastModule,
    HttpModule,
    ChartModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
