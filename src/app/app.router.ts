import  { ModuleWithProviders } from '@angular/core';
import {AuthGuard } from './auth.guard';
import  { RouterModule,Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeachersComponent } from './teachers/teachers.component';
import { StudentsComponent } from './students/students.component';
import { SingleteacherComponent } from './singleteacher/singleteacher.component';
import { SinglestudentComponent } from './singlestudent/singlestudent.component';
import { AdminQueriesComponent } from './admin-queries/admin-queries.component';
import { SingleAdminQueryComponent } from './single-admin-query/single-admin-query.component';
import { NewTeachersComponent } from './new-teachers/new-teachers.component';


const appRoutes:Routes =[
  { path : '', component:  LoginComponent},
  { path: 'dashboard',component: DashboardComponent,canActivate:[AuthGuard]},
  { path: 'teachers',component: TeachersComponent,canActivate:[AuthGuard]},
  { path: 'students',component: StudentsComponent,canActivate:[AuthGuard]},
  { path: 'student/:id',component: SinglestudentComponent,canActivate:[AuthGuard]},
  { path: 'teacher/:name/:email', component:SingleteacherComponent,canActivate:[AuthGuard]},
  { path: 'staqueries', component:AdminQueriesComponent,canActivate:[AuthGuard]},
  { path: 'staqueries/:id', component:SingleAdminQueryComponent,canActivate:[AuthGuard]},
  { path: 'pendingteachers',component:NewTeachersComponent,canActivate:[AuthGuard] }
]

export const AppRouting = RouterModule.forRoot(appRoutes);
