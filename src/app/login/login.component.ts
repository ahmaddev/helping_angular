import { Component, OnInit } from '@angular/core';
import { Router,RouterModule } from '@angular/router';
import { AdminService } from '../providers/admin.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
// import { StudentInterface } from './studentInterface';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  name :string;
  email:string;
  password : string;
  authemail:string;
  authpass:string;
  regLoading:Boolean =false;
  msgs: Message[] = [];
  loginForm : FormGroup;
  regForm : FormGroup;
  constructor(public adminService:AdminService,public router:Router,private messageService: MessageService) {}

  ngOnInit() {
    if(localStorage.user_id){
      this.router.navigate(['/dashboard']);
    }
    this.loginForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)])
    })
    // this.regForm = new FormGroup({
    //   name: new FormControl('',Validators.required),
    //   email :new FormControl('',[Validators.required,Validators.email]),
    //   age: new FormControl('',[Validators.required,Validators.pattern("^[0-9]*$")]),
    //   class: new FormControl('',[Validators.required]),
    //   password : new FormControl('',[Validators.required,Validators.minLength(7)]),
    // })
  }

  authenticate(){
    if(this.loginForm.valid){
      var authdata = {
        email:this.loginForm.value.email,
        password:this.loginForm.value.password
      }
      this.adminService.authenticate(authdata).subscribe((data:any)=>{
        console.log("this is data",data);
        if(data.token){
          localStorage.setItem('token',data.token);
          // localStorage.setItem('user_name',data.userdata.name);
          // localStorage.setItem('user_email',data.userdata.email);
          localStorage.setItem('user_id',data.userdata._id);
          this.router.navigate(['dashboard']);
        }
        // else if(data.status==401) {
        //     this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        // }
      },(error:any)=>{
        if(error.status==401)
        this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        console.log("this is the error",error);
      })
      console.log("this is the auth data",authdata);
    }
  }
}
