import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
@Component({
  selector: 'app-new-teachers',
  templateUrl: './new-teachers.component.html',
  styleUrls: ['./new-teachers.component.css']
})
export class NewTeachersComponent implements OnInit {
  allTeachers:any;
  teacherDp:any;
  noDataFlag:Boolean=false;
    constructor(public adminService :AdminService,public globalService:GlobalService) { }

    ngOnInit() {
      this.getPendingTeachers();
      this.teacherDp =this.globalService.serverip() + 'public/teacher/dp/'

    }


    getPendingTeachers() {
      this.adminService.getPendingTeachers().subscribe((data:any)=>{
        console.log("this is the data",data);
        if(data.data.length){
        this.allTeachers =data.data;
        }
        else {
          this.noDataFlag= true;
        }
      },(error:any)=>{
        console.log("this is the error ",error);
      })
    }

}
