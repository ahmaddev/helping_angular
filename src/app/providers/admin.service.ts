import { Injectable } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
import { GlobalService } from "./global.service";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class AdminService {
  constructor(public http: Http, public globalservice: GlobalService) {}

  authenticate(authdata) {
    return this.http
      .post(this.globalservice.serverip() + "ad/login", authdata)
      .pipe(map(res => res.json()));
  }
  getAllTeachers() {
    return this.http
      .get(this.globalservice.serverip() + "ad/teachers")
      .pipe(map(res => res.json()));
  }
  getSingleTeacher(name, email) {
    return this.http
      .get(this.globalservice.serverip() + "ad/teacher/" + name + "/" + email)
      .pipe(map(res => res.json()));
  }
  getAllStudents() {
    return this.http
      .get(this.globalservice.serverip() + "ad/students")
      .pipe(map(res => res.json()));
  }
  getSingleStudent(id) {
    return this.http
      .get(this.globalservice.serverip() + "ad/student/" + id)
      .pipe(map(res => res.json()));
  }
  enableDisableStudent(id, status) {
    return this.http
      .put(
        this.globalservice.serverip() + "ad/enabledisablestudent/" + id,
        status
      )
      .pipe(map(res => res.json()));
  }
  teacherDegreeApprove(id, data) {
    return this.http
      .put(
        this.globalservice.serverip() + "ad/teacherdegreeapprove/" + id,
        data
      )
      .pipe(map(res => res.json()));
  }

  //  to be made in back end
  enableDisableTeacher(id, status) {
    console.log("Here is you service status", status);
    return this.http
      .put(
        this.globalservice.serverip() + "ad/enabledisableteacher/" + id,
        status
      )
      .pipe(map(res => res.json()));
  }
  getAllAdminQueries() {
    return this.http
      .get(this.globalservice.serverip() + "ad/getalladminqueries")
      .pipe(map(res => res.json()));
  }
  getSingleAdminQuery(id) {
    return this.http
      .get(this.globalservice.serverip() + "ad/getsingleadminquery/" + id)
      .pipe(map(res => res.json()));
  }
  adminReply(id, data) {
    return this.http
      .put(this.globalservice.serverip() + "ad/adminreply/" + id, data)
      .pipe(map(res => res.json()));
  }
  getPendingTeachers() {
    return this.http
      .get(this.globalservice.serverip() + "ad/getpendingteachers")
      .pipe(map(res => res.json()));
  }
  getPendingTeachersCount() {
    return this.http
      .get(this.globalservice.serverip() + "ad/getpendingteacherscount")
      .pipe(map(res => res.json()));
  }
  getTeacherCount() {
    return this.http
      .get(this.globalservice.serverip() + "ad/getteachercount")
      .pipe(map(res => res.json()));
  }
  getStudentCount() {
    return this.http
      .get(this.globalservice.serverip() + "ad/getstudentcount")
      .pipe(map(res => res.json()));
  }
}
