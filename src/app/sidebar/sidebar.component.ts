import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  allTeachers:any;
  teacherDp:any;
  noDataFlag:Boolean=false;
  count:number =0;
  constructor(public adminService :AdminService,public globalService:GlobalService) { }

  ngOnInit() {
    this.getPendingTeachers();
  }

  getPendingTeachers() {
    this.adminService.getPendingTeachersCount().subscribe((data:any)=>{
      console.log("this is the data",data);
      this.count = data.data;
      // if(data.data){
      //   this.noDataFlag  = false;
      // this.allTeachers =data.data;
      // }
      // else {
      //   this.noDataFlag= true;
      // }
    },(error:any)=>{
      console.log("this is the error ",error);
    })
  }
}
