import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleAdminQueryComponent } from './single-admin-query.component';

describe('SingleAdminQueryComponent', () => {
  let component: SingleAdminQueryComponent;
  let fixture: ComponentFixture<SingleAdminQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleAdminQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleAdminQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
