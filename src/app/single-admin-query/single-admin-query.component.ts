import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../providers/global.service';
import { AdminService } from '../providers/admin.service';
import { Router,ActivatedRoute,RouterModule } from '@angular/router';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-single-admin-query',
  templateUrl: './single-admin-query.component.html',
  styleUrls: ['./single-admin-query.component.css']
})
export class SingleAdminQueryComponent implements OnInit {
  singleQueryData:any;
  qryForm : FormGroup;
  querySentLoading:boolean=false;
  msgs:Message[]=[];
  queryId:any;
  constructor(public globalService:GlobalService,public adminService:AdminService,public activatedRoute:ActivatedRoute,private messageService: MessageService) { }

  ngOnInit() {
    this.queryId = this.activatedRoute.snapshot.paramMap.get('id');
    this.qryForm = new FormGroup({
      replytext: new FormControl('',Validators.required)
    })
    this.getSingleAdminQuery();
  }

  getSingleAdminQuery() {
    this.adminService.getSingleAdminQuery(this.queryId).subscribe((data:any)=>{
      this.singleQueryData = data.data[0];
      console.log("this is the data",data);
    },(error:any)=>{
      console.log("this is the error",error);
    })
  }
  adminReply() {
    this.querySentLoading=true;
    var replyData = {
      userType : 'admin',
      replyText: this.qryForm.value.replytext,
      repliedAt : new Date()
    }
    this.adminService.adminReply(this.queryId,replyData).subscribe((data:any)=>{
      console.log("this is the admin reply",data);
      this.messageService.add({severity:'success', summary:'Query replied'});
      this.qryForm.reset();
      this.getSingleAdminQuery();
      this.querySentLoading=false;
    },(error:any)=>{
      console.log("this is error",error);
    })
  }

}
