import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';
import { GlobalService } from '../providers/global.service';
import { RouterModule,Router,ActivatedRoute } from '@angular/router';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-singlestudent',
  templateUrl: './singlestudent.component.html',
  styleUrls: ['./singlestudent.component.css']
})
export class SinglestudentComponent implements OnInit {
  studentData:any;
  studentDp:any;
  id:any;
  docUrl:any;
  dpUrl:any;
  msgs : Message[]=[];
    constructor(public adminService :AdminService,public globalService:GlobalService,public activatedRoute:ActivatedRoute,private messageService: MessageService) {
      this.id= this.activatedRoute.snapshot.paramMap.get('id');
      this.dpUrl =this.globalService.serverip()  + 'public/student/dp/';
    }

    ngOnInit() {
      this.getSingleStudent();
      this.studentDp =this.globalService.serverip() + 'public/student/dp/'

    }


    getSingleStudent() {
      this.adminService.getSingleStudent(this.id).subscribe((data:any)=>{
        console.log("this is the data",data);
        if(data.status==200){
        this.studentData =data.data;
        }
      },(error:any)=>{
        console.log("this is the error ",error);
      })
    }

    // teacherDegreeApprove(id) {
    //   this.adminService.teacherDegreeApprove(id).subscribe((data:any)=>{
    //     console.log("thisi is the data",data);
    //     this.messageService.add({severity:'success', summary:'Degree Approved'});
    //     this.getSingleStudent();
    //   },(error:any)=>{
    //     console.log("this is the error ",error);
    //   })
    // }
    enableDisableStudent(id,status) {
      var statusData ={
        isEnabled:status
      }
      this.adminService.enableDisableStudent(id,statusData).subscribe((data:any)=>{
        if(status == false){
          this.getSingleStudent();
          this.messageService.add({severity:'success', summary:'Student De-Activated'});
        }
        else {
          this.getSingleStudent();
          this.messageService.add({severity:'success', summary:'Student Activated'});
        }
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }

}
