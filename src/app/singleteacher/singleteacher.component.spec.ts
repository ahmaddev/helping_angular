import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleteacherComponent } from './singleteacher.component';

describe('SingleteacherComponent', () => {
  let component: SingleteacherComponent;
  let fixture: ComponentFixture<SingleteacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleteacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleteacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
